var electron = require('electron');
var app = electron.app;
var BrowserWindow = electron.BrowserWindow;
var Menu = electron.Menu;
var MenuItem = electron.MenuItem;

var path = require('path');

var mainWindow = null;

app.on('window-all-closed', function () {
  if (process.platform != 'darwin') {
    app.quit();
  }
});

app.on('ready', function () {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: false,
      preload: path.join(__dirname, 'expose-window-apis.js')
    }
  });

  mainWindow.loadURL('file://' + __dirname + '/index.html');

  var template = [
    {
      label: "File",
      submenu: [
        {
          label: "Developer Tools",
          click: function (a, b) {
            mainWindow.webContents.openDevTools();
          }
        }
      ]
    }
  ];
  
  var menu = Menu.buildFromTemplate(template);

  mainWindow.setMenu(menu);

  mainWindow.on('closed', function () {
    mainWindow = null;
  });

});
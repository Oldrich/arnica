class Model extends Lang {

    public barthelNormal: Barthel;
    public mmse: MMSE;
    public mmseDoctor: MmseDoctor;

    public constructor() {
        super();
        var o = this;
        o.barthelNormal = new Barthel(o);
        o.mmse = new MMSE(o);
        o.mmseDoctor = new MmseDoctor(o);
    }

    public addTheme = (name: string) => {
        var o = this;
        return () => {
            $("body").attr("class", "");
            $("body").addClass(name);
        };
    }

    public callHelp = () => {
        app.popup(".popup-helpCalled");
    }
}
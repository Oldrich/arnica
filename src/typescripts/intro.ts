class Intro {

    public constructor() {

    }

    public show = (left: number, top: number, width: number, height: number, padding: number) => {
        var o = this;
        var el = $('.intro-overlay');
        if (el.css('display') === 'none') {
            el.css("top", 0);
            el.css("left", 0);
            el.css("width", 0);
            el.css("height", 0);
            el.fadeIn(500, () => {
                o.show(left, top, width, height, padding);
            });
        } else {
            el.css("top", top - padding);
            el.css("left", left - padding);
            el.css("width", width + 2 * padding);
            el.css("height", height + 2 * padding);
        }
    }

    public hide = () => {
        var el = $('.intro-overlay');
        el.fadeOut(500);
    }

}
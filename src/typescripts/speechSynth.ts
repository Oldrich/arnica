class SpeechSynth {

    public speechRec: SpeechRec;
    private utter;
    private speed;

    public constructor() {
        speechSynthesis.getVoices();
    }

    public whenReady = (func) => {
        var fired = false;
        speechSynthesis.onvoiceschanged = function () {
            if (!fired) {
                fired = true;
                func();
            }
        };
    }

    public play = (lang: string, index: number, rate: number, pitch: number, text: string) => {
        var o = this;
        var isSpeechRec = false;
        if (o.speechRec) {
            isSpeechRec = o.speechRec.isRunning;
            o.speechRec.stop();
        }
        var def = $.Deferred<void>();
        var texts = text.split(/(-[ws]\d+-)/).filter((v) => {
            return v.trim().length > 0; 
        });
        o.playArr(lang, index, rate, pitch, texts, () => {
            if (isSpeechRec) {
                o.speechRec.start();
            }
            setTimeout(function() {
                def.resolve();
            }, 100);
        });
        return def.promise();
    }

    private playArr = (lang: string, index: number, rate: number, pitch: number, texts: string[], then) => {
        var o = this;
        var t = texts[0];
        if (t.indexOf("-w") > -1) {
            var m = t.match(/-w(\d+)-/);
            setTimeout(function () {
                if (texts.length > 1) {
                    o.playArr(lang, index, rate, pitch, texts.slice(1), then);
                } else {
                    then();
                }
            }, parseInt(m[1]));
        } else if (t.indexOf("-s") > -1) {
            var m = t.match(/-s(\d+)-/);
            o.speed = parseInt(m[1]);
            if (texts.length > 1) {
                o.playArr(lang, index, rate, pitch, texts.slice(1), then);
            } else {
                then();
            }
        } else {
            var voice = o.getVoice(lang, index);
            o.utter = new SpeechSynthesisUtterance(t);
            o.utter.voice = voice;
            o.utter.rate = rate;
            if (o.speed) {
                o.utter.rate = rate * o.speed / 100.0;
            }
            o.utter.pitch = pitch;
            o.utter.onend = (event) => {
                if (texts.length > 1) {
                    o.playArr(lang, index, rate, pitch, texts.slice(1), then);
                } else {
                    then();
                }
            };
            speechSynthesis.speak(o.utter);
        }
    }

    private getVoice = (lang: string, index: number) => {
        var voices = speechSynthesis.getVoices();
        var vs = [];
        for (var i = 0; i < voices.length; i++) {
            if (voices[i].lang === lang) {
                vs.push(voices[i]);
            }
        }
        return vs[index];
    }

}
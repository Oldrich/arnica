declare var abc;

class SpeechRec {

    public rec;

    public onNotUnderstanding;

    private keywords: { [setId: string]: { [commandId: string]: string[] } } = {};
    private callbacks: { [setId: string]: (commandIds: string[]) => void } = {};
    public isRunning = false;

    constructor(private m: Model) {
        var o = this;
        o.rec = new webkitSpeechRecognition();
        o.rec.continuous = true;
        o.rec.maxAlternatives = 4;

        o.rec.onresult = o.onResult;
        o.rec.onstart = o.onStart;
        o.rec.onerror = o.onError;
        o.rec.onend = o.onEnd;
    }

    public start = () => {
        var o = this;
        o.stop();
        o.rec.lang = o.m.langID();
        try {
            o.rec.start();
        } catch (e) {
            console.log("speech rec start error");
        }
        o.isRunning = true;
    }

    public stop = () => {
        var o = this;
        o.rec.abort();
        o.isRunning = false;
    }

    public subscribe = (setId: string, keywords: { [commandId: string]: string[] }, callback: (commandIds: string[]) => void) => {
        var o = this;
        o.keywords[setId] = keywords;
        o.callbacks[setId] = callback;
    }

    public unsubscribe = (setId: string) => {
        var o = this;
        delete o.keywords[setId];
        delete o.callbacks[setId];
    }

    private onStart = () => {
        console.log("onStart");
    }

    private onEnd = () => {
        console.log("onEnd");
        var o = this;
        if (o.isRunning) {
            o.rec.start();
        }
    }

    private onError = (event) => {
        console.log(event);
    }

    private onResult = (event) => {
        var o = this;
        if (o.isRunning) {
            abc = event.results;
            console.log(event.results);
            o.analyzeResults(event.results);
        }
    }

    private analyzeResults = (results: any[][]) => {
        var o = this;
        var keywords = {};
        var i = results.length - 1;
        for (var j = 0; j < results[i].length; j++) {
            var result = results[i][j];
            var found = o.detectKeywords(result.transcript, keywords);
            if (found) {
                break;
            }
        }
        var keys = Object.keys(keywords);
        if (keys.length > 0) {
            if (keys.length > 1) {
                if (o.onNotUnderstanding) {
                    o.onNotUnderstanding(keywords);
                }
            } else {
                var commandIds = Object.keys(keywords[keys[0]]);
                if (commandIds.length === 1) {
                    o.callbacks[keys[0]](commandIds);
                } else if (o.onNotUnderstanding) {
                    o.onNotUnderstanding(keywords);
                }
            }
        }
    }

    private detectKeywords = (text: string, keywords: any) => {
        var o = this;
        var found = false;
        for (var setId in o.keywords) {
            var set = o.keywords[setId];
            for (var commandId in set) {
                var command = set[commandId];
                command.forEach((keyword) => {
                    var regex = new RegExp('\\b' + keyword.toLowerCase() + '\\b');
                    var i = text.toLowerCase().search(regex);
                    if (i >= 0) {
                        if (!keywords[setId]) {
                            keywords[setId] = {};
                        }
                        keywords[setId][commandId] = true;
                        found = true;
                    }
                });
            }
        }
        return found;
    }

}
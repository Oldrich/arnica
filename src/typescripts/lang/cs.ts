var languages;

languages = languages || {};

languages["cs"] = {
    speechID: "cs-CZ",
    name: "Česky",
    back: "Zpět",
    chooseOption: "Nejprve prosím zvolte jednu z uvedených možností.",
    mainScreen: {
        title: "Hlavní stránka",
        barthelIndexNormalCard: "Bartelův Dotazník",
        mmseTestNormalCard: "MMSE Dotazník",
        testSeq1NormalCard: "Seznam testů 1",
    },
    barthelIndexNormal: {
        title: "Barthel Index",
        goNext: "Další otázka",
        thankYou: "Děkujeme za vyplnění testu",
        goHome: "Jít na hlavní stránku",
        questions: [
            {
                question: "Jsi schopný se sám najíst?",
                answers: [
                    {
                        text: "Ano",
                        value: 10,
                        backgroundColor: "#0074D9",
                    },
                    {
                        text: "S pomocí",
                        value: 5,
                        backgroundColor: "#FF851B",
                    },
                    {
                        text: "Ne",
                        value: 0,
                        backgroundColor: "#3D9970",
                    }
                ]
            },

            {
                question: "Jsi schopný se sám vysprchovat?",
                answers: [
                    {
                        text: "Ano",
                        value: 10,
                        backgroundColor: "#0074D9",
                    },
                    {
                        text: "S pomocí",
                        value: 5,
                        backgroundColor: "#FF851B",
                    },
                    {
                        text: "Ne",
                        value: 0,
                        backgroundColor: "#3D9970",
                    }
                ]
            },

            {
                question: "Jsi schopný sám provést osobní hygienu?",
                answers: [
                    {
                        text: "Ano",
                        value: 10,
                        backgroundColor: "#0074D9",
                    },
                    {
                        text: "S pomocí",
                        value: 5,
                        backgroundColor: "#FF851B",
                    },
                    {
                        text: "Ne",
                        value: 0,
                        backgroundColor: "#3D9970",
                    }
                ]
            },

            {
                question: "Jsi schopný se sám obléci a svléci?",
                answers: [
                    {
                        text: "Ano",
                        value: 10,
                        backgroundColor: "#0074D9",
                    },
                    {
                        text: "S pomocí",
                        value: 5,
                        backgroundColor: "#FF851B",
                    },
                    {
                        text: "Ne",
                        value: 0,
                        backgroundColor: "#3D9970",
                    }
                ]
            },

            {
                question: "Máš problémy s úniky moči?",
                answers: [
                    {
                        text: "Ano",
                        value: 10,
                        backgroundColor: "#0074D9",
                    },
                    {
                        text: "Sometimes",
                        value: 5,
                        backgroundColor: "#FF851B",
                    },
                    {
                        text: "Ne",
                        value: 0,
                        backgroundColor: "#3D9970",
                    }
                ]
            },

            {
                question: "Máš problémy se střevními úniky?",
                answers: [
                    {
                        text: "Ano",
                        value: 10,
                        backgroundColor: "#0074D9",
                    },
                    {
                        text: "Sometimes",
                        value: 5,
                        backgroundColor: "#FF851B",
                    },
                    {
                        text: "Ne",
                        value: 0,
                        backgroundColor: "#3D9970",
                    }
                ]
            },

            {
                question: "Jsi schopný si sám dojít na toiletu?",
                answers: [
                    {
                        text: "Ano",
                        value: 10,
                        backgroundColor: "#0074D9",
                    },
                    {
                        text: "S pomocí",
                        value: 5,
                        backgroundColor: "#FF851B",
                    },
                    {
                        text: "Ne",
                        value: 0,
                        backgroundColor: "#3D9970",
                    }
                ]
            },

            {
                question: "Jsi schopný se sám přesunout z postele na židli a zpět?",
                answers: [
                    {
                        text: "Ano",
                        value: 10,
                        backgroundColor: "#0074D9",
                    },
                    {
                        text: "S pomocí",
                        value: 5,
                        backgroundColor: "#FF851B",
                    },
                    {
                        text: "Ne",
                        value: 0,
                        backgroundColor: "#3D9970",
                    }
                ]
            },

            {
                question: "Jsi schopný sám chodit po bytě?",
                answers: [
                    {
                        text: "Ano",
                        value: 10,
                        backgroundColor: "#0074D9",
                    },
                    {
                        text: "S pomocí",
                        value: 5,
                        backgroundColor: "#FF851B",
                    },
                    {
                        text: "Ne",
                        value: 0,
                        backgroundColor: "#3D9970",
                    }
                ]
            },

            {
                question: "Jsi schopný se sám jít do schodů?",
                answers: [
                    {
                        text: "Ano",
                        value: 10,
                        backgroundColor: "#0074D9",
                    },
                    {
                        text: "S pomocí",
                        value: 5,
                        backgroundColor: "#FF851B",
                    },
                    {
                        text: "Ne",
                        value: 0,
                        backgroundColor: "#3D9970",
                    }
                ]
            }
        ]
    },
    rightPanel: {
        lang: "Jazyk",
        colorScheme: "Barvy",
        colorSchemeDefault: "Původní",
        colorSchemeDark: "Tmavé",
        colorSchemeOrange: "Oranžové",
        colorSchemeGreen: "Zelené"
    },
    toolbar: {
        callHelp: "Zavolat Okamžitou Pomoc!"
    },
    callHelp: {
        areYouSure: "Jste si jistý, že chcete zavolat okamžitou pomoc?",
        yes: "Ano",
        no: "Ne",
        helpCalled: "Okamžitá pomoc byla zavolána!",
        closeWindow: "Zavřít toto okno"
    }

}
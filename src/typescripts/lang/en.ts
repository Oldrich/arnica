var languages;

languages = languages || {};

languages["en"] = {
    speechSynth: {
        lang: "en-GB",
        id: 1,
        rate: 1, 
        pitch: 1
    },
    name: "English",
    back: "Back",
    chooseOption: "Please select an option first",
    mainScreen: {
        title: "Main Screen",
        barthelIndexNormalCard: "Barthel Index",
        mmseTestNormalCard: "MMSE Test",
        testSeq1NormalCard: "Playlist 1",
    },
    mmseDoctor: {
        title: "MMSE - Doctor View",
        previousTask: "Previous Task",
        nextTask: "Next Task",
        question: "Task Name",
        answer: "Correct Answer",
        points: "Number of points",
        totalPoints: "Total number of points: ",
        close: "Close"
    },
    mmse: {
        title: "MMSE - Patient View",
        synth: {
            welcome: "Welcome to the mini mental state examination test. -w200- I am really glad to see you! -w400-",
            welcome2: "Let me begin by showing you how to navigate through the test. -w300-",
            bullets: "This test consists of a set of tasks. -w300- Bullets at the bottom of the screen indicate progress through out the test. -w300-",
            question: "I will always ask you a question or ask you to do something. -w300- Your response will be registered by my colleague. -w300- As soon as my colleague is happy with your response, we will move on to the next task.",
            emergency: "In case of emergency, you can click on the link in the bottom of the screen or on the big button at the robot. This will immediately call for emergency help. Please do not misuse the button.",
            ready: "Now you are ready to begin the test. Go ahead by fulfilling the first task.",
            questionNumber: "Task number",
            goNextBtn: "I am moving on to the next task.",
            goPreviousBtn: "I am moving on to the previous task."
        },
        questions: [
            // {
            //     title: "What is the current year?",
            //     bodyID: ""
            // },
            {
                title: "What is the current season?",
                bodyID: ""
            },
            // {
            //     title: "What is the current date?",
            //     bodyID: ""
            // },
            // {
            //     title: "What is the current day?",
            //     bodyID: ""
            // },
            // {
            //     title: "What is the current month?",
            //     bodyID: ""
            // },
            // {
            //     title: "What is the current state?",
            //     bodyID: ""
            // },
            // {
            //     title: "What is the current country?",
            //     bodyID: ""
            // },
            {
                title: "What is the current town?",
                bodyID: ""
            },
            // {
            //     title: "What is the current hospital?",
            //     bodyID: ""
            // },
            // {
            //     title: "What is the current floor?",
            //     bodyID: ""
            // },
            {
                title: "Name these three objects",
                synthIntro: "Now I will mention three objects. -w300- Listen carefully and try to remember them. -w300-",
                synthTask: "The words are -w1000- -s70- table -w1000- robot -w1000- chair. -s100- -w1000- Can you please repeat the words loud?",
                bodyID: ""
            },
            {
                title: "Repeatedly subtract 7 starting from 100.",
                synthTask: "I would like you to repeatedly subtract 7 from 100. -w500- As an example, I start by saying -w1000- 100 minus 7 is 93 -w1000- Now go ahead and continue subtracting! 93 minus 7 is...",
                bodyID: ""
            },
            {
                title: "Repeat the three objects mentioned earlier.",
                synthTask: "Can you please repeat the three object I told you earlier?",
                bodyID: ""
            },
            {
                title: "Name these two objects",
                synthTask: "Tell me please names of the two objects that I show you on the screen. -w500- Please be accurate!",
                bodyID: "#mmseNameObjects"
            },
            {
                title: "Repeat the following: “No ifs, ands, or buts”",
                synthTask: "Repeat the following: -s70- “No -w10- ifs, -w10- ands, -w10- or -w10- buts” -s100- ",
                bodyID: ""
            },
            {
                title: "Follow the command I give you",
                synthIntro: "Now I will give you a command. -w300- Listen please carefully and do exactly as I ask you. -w1000-",
                synthTask: "-s80- Take a paper in your hand, fold it in half, and put it on the floor. -s100-",
                bodyID: ""
            },
            {
                title: "Read the command below carefully and do exatly as you are asked to.",
                bodyID: "#mmseReadAndObey"
            },
            {
                title: "Write a sentence",
                synthTask: "My colleague will give you a piece of paper. -w500- May I ask you to write any complete sentence on this piece of paper for me!",
                body: ""
            },
            {
                title: "Copy this drawing",
                synthTask: "My colleague will give you a paper with a drawing. -w300- Please copy the drawing on the same piece of paper!",
                body: ""
            },
            {
                title: "Congratulations! You made it!",
                synthTask: "Congratulations!!! -w300- You made it to the end of the test!!!",
                body: ""
            }
        ]
    },
    barthelIndexNormal: {
        synthCommands: {
            nextQuestion: ["next", "go on", "move on"],
            previousQuestion: ["previous", "back"],
            repeatQuestion: ["repeat", "again", "replay"]
        },
        synth: {
            welcome: "Welcome to the Barthel test. -w200- I am really glad to see you! -w400-",
            welcome2: "Let me begin by showing you how to navigate through the test. -w300-",
            bullets: "This test consists of a set of questions. -w300- Bullets at the bottom of the screen indicate progress through out the test. -w300-",
            question: "I will always ask you a question. -w300-",
            answers: "After the question, I will tell you the possible answers. -w300- You can choose only one of the answers. -w300- The answer can be selected either by touching the respective button or by saying the answer loud. -w300- You are welcome to answer by a whole sentence. -w300- I will do my best to understand and will then mark the answer for you. -w300-",
            goNext: "When you are satisfied with the selected answer click on the button to go to then next question. -w300- You can also ask me to click on the button for you.",
            emergency: "In case of emergency, you can click on the link in the bottom of the screen or on the big button at the robot. This will immediately call for emergency help. Please do not misuse the button.",
            ready: "Now you are ready to begin the test. Go ahead by answering the first question.",
            questionNumber: "Question number",
            possibleAnsers: "Possible answers are",
            selectedAnswer: "You have selected answer number",
            goNextBtn: "I am moving on to the next question.",
            goPreviousBtn: "I am moving on to the previous question.",
            notUnderstanding: "I am really sorry but I recognized multiple commands or answers and I do not know which one to choose."

        },
        title: "Barthel Index",
        goNext: "Go to the next question",
        thankYou: "Thank you for filling in the test.",
        goHome: "Go to the Home Page",
        questions: [

            // {
            //     question: "Are you able to <em>feed</em> yourself?",
            //     answers: [
            //         {
            //             text: "Yes",
            //             value: 10,
            //             backgroundColor: "#0074D9",
            //             icon: "fa fa-snapchat-ghost",
            //             speechRecAnswers: ["yes", "first", "one", "1"]
            //         },
            //         {
            //             text: "With help",
            //             value: 5,
            //             backgroundColor: "#FF851B",
            //             icon: "fa fa-anchor",
            //             speechRecAnswers: ["help", "second", "two", "2"]
            //         },
            //         {
            //             text: "No",
            //             value: 0,
            //             backgroundColor: "#3D9970",
            //             icon: "fa fa-american-sign-language-interpreting",
            //             speechRecAnswers: ["no", "not", "third", "three", "3"]
            //         }
            //     ]
            // },

            // {
            //     question: "Are you able to <em>take a bath or shower</em> yourself?",
            //     answers: [
            //         {
            //             text: "Yes",
            //             value: 10,
            //             backgroundColor: "#0074D9",
            //             icon: "fa fa-snapchat-ghost",
            //             speechRecAnswers: ["yes", "first", "one", "1"]
            //         },
            //         {
            //             text: "With help",
            //             value: 5,
            //             backgroundColor: "#FF851B",
            //             icon: "fa fa-anchor",
            //             speechRecAnswers: ["help", "second", "two", "2"]
            //         },
            //         {
            //             text: "No",
            //             value: 0,
            //             backgroundColor: "#3D9970",
            //             icon: "fa fa-american-sign-language-interpreting",
            //             speechRecAnswers: ["no", "not", "third", "three", "3"]
            //         }
            //     ]
            // },

            // {
            //     question: "Are you able to do the <em>personal care</em> yourself?",
            //     answers: [
            //         {
            //             text: "Yes",
            //             value: 10,
            //             backgroundColor: "#0074D9",
            //             icon: "fa fa-snapchat-ghost",
            //             speechRecAnswers: ["yes", "first", "one", "1"]
            //         },
            //         {
            //             text: "With help",
            //             value: 5,
            //             backgroundColor: "#FF851B",
            //             icon: "fa fa-anchor",
            //             speechRecAnswers: ["help", "second", "two", "2"]
            //         },
            //         {
            //             text: "No",
            //             value: 0,
            //             backgroundColor: "#3D9970",
            //             icon: "fa fa-american-sign-language-interpreting",
            //             speechRecAnswers: ["no", "not", "third", "three", "3"]
            //         }
            //     ]
            // },

            // {
            //     question: "Are you able to get <em>dressed and undressed</em> yourself?",
            //     answers: [
            //         {
            //             text: "Yes",
            //             value: 10,
            //             backgroundColor: "#0074D9",
            //             icon: "fa fa-snapchat-ghost",
            //             speechRecAnswers: ["yes", "first", "one", "1"]
            //         },
            //         {
            //             text: "With help",
            //             value: 5,
            //             backgroundColor: "#FF851B",
            //             icon: "fa fa-anchor",
            //             speechRecAnswers: ["help", "second", "two", "2"]
            //         },
            //         {
            //             text: "No",
            //             value: 0,
            //             backgroundColor: "#3D9970",
            //             icon: "fa fa-american-sign-language-interpreting",
            //             speechRecAnswers: ["no", "not", "third", "three", "3"]
            //         }
            //     ]
            // },

            // {
            //     question: "Do you have problems with <em>urine incontinence</em>?",
            //     answers: [
            //         {
            //             text: "Yes",
            //             value: 10,
            //             backgroundColor: "#0074D9",
            //             icon: "fa fa-snapchat-ghost",
            //             speechRecAnswers: ["yes", "first", "one", "1"]
            //         },
            //         {
            //             text: "Sometimes",
            //             value: 5,
            //             backgroundColor: "#FF851B",
            //             icon: "fa fa-anchor",
            //             speechRecAnswers: ["sometimes"]
            //         },
            //         {
            //             text: "No",
            //             value: 0,
            //             backgroundColor: "#3D9970",
            //             icon: "fa fa-american-sign-language-interpreting",
            //             speechRecAnswers: ["no", "not", "third", "three", "3"]
            //         }
            //     ]
            // },

            {
                question: "Do you have problems with <em>bladder incontinence</em>?",
                answers: [
                    {
                        text: "Yes",
                        value: 10,
                        backgroundColor: "#0074D9",
                        icon: "fa fa-snapchat-ghost",
                        speechRecAnswers: ["yes", "first", "one", "1"]
                    },
                    {
                        text: "Sometimes",
                        value: 5,
                        backgroundColor: "#FF851B",
                        icon: "fa fa-anchor",
                        speechRecAnswers: ["sometimes"]
                    },
                    {
                        text: "No",
                        value: 0,
                        backgroundColor: "#3D9970",
                        icon: "fa fa-american-sign-language-interpreting",
                        speechRecAnswers: ["no", "not", "third", "three", "3"]
                    }
                ]
            },

            {
                question: "Are you able to use the <em>toilet</em> yourself?",
                answers: [
                    {
                        text: "Yes",
                        value: 10,
                        backgroundColor: "#0074D9",
                        icon: "fa fa-snapchat-ghost",
                        speechRecAnswers: ["yes", "first", "one", "1"]
                    },
                    {
                        text: "With help",
                        value: 5,
                        backgroundColor: "#FF851B",
                        icon: "fa fa-anchor",
                        speechRecAnswers: ["help", "second", "two", "2"]
                    },
                    {
                        text: "No",
                        value: 0,
                        backgroundColor: "#3D9970",
                        icon: "fa fa-american-sign-language-interpreting",
                        speechRecAnswers: ["no", "not", "third", "three", "3"]
                    }
                ]
            },

            {
                question: "Are you able to <em>move</em> yourself from <em>bed to chair</em> and back?",
                answers: [
                    {
                        text: "Yes",
                        value: 10,
                        backgroundColor: "#0074D9",
                        icon: "fa fa-snapchat-ghost",
                        speechRecAnswers: ["yes", "first", "one", "1"]
                    },
                    {
                        text: "With help",
                        value: 5,
                        backgroundColor: "#FF851B",
                        icon: "fa fa-anchor",
                        speechRecAnswers: ["help", "second", "two", "2"]
                    },
                    {
                        text: "No",
                        value: 0,
                        backgroundColor: "#3D9970",
                        icon: "fa fa-american-sign-language-interpreting",
                        speechRecAnswers: ["no", "not", "third", "three", "3"]
                    }
                ]
            },

            {
                question: "Are you able to <em>walk</em> at home yourself?",
                answers: [
                    {
                        text: "Yes",
                        value: 10,
                        backgroundColor: "#0074D9",
                        icon: "fa fa-snapchat-ghost",
                        speechRecAnswers: ["yes", "first", "one", "1"]
                    },
                    {
                        text: "With help",
                        value: 5,
                        backgroundColor: "#FF851B",
                        icon: "fa fa-anchor",
                        speechRecAnswers: ["help", "second", "two", "2"]
                    },
                    {
                        text: "No",
                        value: 0,
                        backgroundColor: "#3D9970",
                        icon: "fa fa-american-sign-language-interpreting",
                        speechRecAnswers: ["no", "not", "third", "three", "3"]
                    }
                ]
            },

            {
                question: "Are you able to go to the stairs yourself?",
                answers: [
                    {
                        text: "Yes",
                        value: 10,
                        backgroundColor: "#0074D9",
                        icon: "fa fa-snapchat-ghost",
                        speechRecAnswers: ["yes", "first", "one", "1"]
                    },
                    {
                        text: "With help",
                        value: 5,
                        backgroundColor: "#FF851B",
                        icon: "fa fa-anchor",
                        speechRecAnswers: ["help", "second", "two", "2"]
                    },
                    {
                        text: "No",
                        value: 0,
                        backgroundColor: "#3D9970",
                        icon: "fa fa-american-sign-language-interpreting",
                        speechRecAnswers: ["no", "not", "third", "three", "3"]
                    }
                ]
            }
        ]
    },
    rightPanel: {
        lang: "Language",
        colorScheme: "Colors",
        colorSchemeDefault: "Default",
        colorSchemeDark: "Dark",
        colorSchemeOrange: "Orange",
        colorSchemeGreen: "Green",
        mmseDoctor: "MMSE Doctor"
    },
    toolbar: {
        callHelp: "Call Immediate Help!"
    },
    callHelp: {
        areYouSure: "Are you sure you wish to call for an immediate help?",
        yes: "Yes",
        no: "No",
        helpCalled: "Immediate help has been called!",
        closeWindow: "Close this window"
    }
}
class FireworksParticle {

    public pos: { x: number; y: number };

    public vel = { x: 0, y: 0 };
    public shrink = 0.97;
    public size = 2;

    public resistance = 1;
    public gravity = 0;

    public flick = false;

    public alpha = 1;
    public fade = 0;
    public color = 0;

    public constructor(pos: { x: number; y: number }) {
        var o = this;
        o.pos = {
            x: pos ? pos.x : 0,
            y: pos ? pos.y : 0
        };
    }

    public update = () => {
        var o = this;
        // apply resistance
        o.vel.x *= o.resistance;
        o.vel.y *= o.resistance;

        // gravity down
        o.vel.y += o.gravity;

        // update position based on speed
        o.pos.x += o.vel.x;
        o.pos.y += o.vel.y;

        // shrink
        o.size *= o.shrink;

        // fade out
        o.alpha -= o.fade;
    };

    public render = (c: CanvasRenderingContext2D) => {
        var o = this;
        if (!o.exists()) {
            return;
        }

        c.save();

        c.globalCompositeOperation = 'lighter';

        var x = o.pos.x,
            y = o.pos.y,
            r = o.size / 2;

        var gradient = c.createRadialGradient(x, y, 0.1, x, y, r);
        gradient.addColorStop(0.1, "rgba(255,255,255," + o.alpha + ")");
        gradient.addColorStop(0.8, "hsla(" + o.color + ", 100%, 50%, " + o.alpha + ")");
        gradient.addColorStop(1, "hsla(" + o.color + ", 100%, 50%, 0.1)");

        c.fillStyle = gradient;

        c.beginPath();
        c.arc(o.pos.x, o.pos.y, o.flick ? Math.random() * o.size : o.size, 0, Math.PI * 2, true);
        c.closePath();
        c.fill();

        c.restore();
    };

    public exists = () => {
        var o = this;
        return o.alpha >= 0.1 && o.size >= 1;
    };

}






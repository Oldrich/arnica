class MMSE {

    private swiper;

    public constructor(private m) {
        var o = this;
        o.setF7EventHooks();
    }

    // public destructor = () => {
    //     var o = this;
    //     speechRec.unsubscribe("mmseSynthCommands");
    // }

    private listenToPubNub = () => {
        var o = this;
        pubnub.subscribe({
            channel: 'command',
            callback: function (command) {
                if (command == "next") {
                    o.goNext();
                } else if (command == "previous") { 
                    var s = o.m.lang().speechSynth;
                    var synth = o.m.lang().mmse.synth;
                    speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.goPreviousBtn).done(() => {
                        o.swiper.slidePrev();
                    });
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    }

    public back = () => {
        var o = this;
        var s = o.m.lang().speechSynth;
        var synth = o.m.lang().mmse.synth;
        if (o.swiper.activeIndex > 0) {
            speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.goPreviousBtn).done(() => {
                o.swiper.slidePrev();
            });
        } else {
            pubnub.unsubscribe({
                channel: 'command',
            });
            mainView.router.back();
        }
    }

    public goNext = () => {
        var o = this;
        var s = o.m.lang().speechSynth;
        var synth = o.m.lang().mmse.synth;
        speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.goNextBtn).done(() => {
            o.swiper.slideNext();
        });
    }

    private setF7EventHooks = () => {
        var o = this;
        app.onPageInit('mmse', (page) => {
            o.onPageInit();
            o.onSlideChangeBegin();
        });
        app.onPageAfterAnimation('mmse', (page) => {
            o.onPageAfterAnimation();
        });
    }

    private onPageInit = () => {
        var o = this;
        o.swiper = app.swiper('.mmse .swiper-container', {
            pagination: '.mmse .swiper-pagination',
            paginationHide: false,
            paginationClickable: false,
            onlyExternal: true,
            speed: 500
        });
        o.swiper.on("slideChangeEnd", o.onSlideChangeEnd);
        o.swiper.on("slideChangeStart", o.onSlideChangeBegin);
        o.listenToPubNub();
    }

    private onPageAfterAnimation = () => {
        var o = this;
        pubnub.publish({
            channel: 'index',
            message: o.swiper.activeIndex + 1
        });
        //o.ask();
        o.welcomeSynth().then(() => {
            o.ask();
        });
    }

    private onSlideChangeEnd = () => {
        var o = this;
        pubnub.publish({
            channel: 'index',
            message: o.swiper.activeIndex + 1
        });
        o.ask();
    }

    private onSlideChangeBegin = () => {
        var o = this;
        var index: number = o.swiper.activeIndex;
        var q = o.m.lang().mmse.questions[index];
        var qel = $('.mmse .swiper-slide-active.testSlide .questionMMSE');
        var qan = $('.mmse .swiper-slide-active.testSlide .answersMMSE');
        qan.html($(q.bodyID).html());
        if (q.bodyID) {
            qel.addClass("testQuestion");
            qel.removeClass("testQuestionMMSE");
            qan.addClass("testAnswers");
            qan.removeClass("testAnswersMMSE");
        } else {
            qel.removeClass("testQuestion");
            qel.addClass("testQuestionMMSE");
            qan.removeClass("testAnswers");
            qan.addClass("testAnswersMMSE");
        }
    }

    private welcomeSynth = () => {
        var o = this;
        var def = $.Deferred();
        if (o.m.lang().speechSynth) {
            var s = o.m.lang().speechSynth;
            var synth = o.m.lang().mmse.synth;

            speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.welcome).done(() => {
                intro.show(0, 0, 0, 0, 0);
                speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.welcome2).done(() => {
                    o.highlightBullets();
                    speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.bullets).done(() => {
                        o.highlightQuestion();
                        speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.question).done(() => {
                            o.highlightEmergency();
                            speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.emergency).done(() => {
                                intro.hide();
                                speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.ready).done(() => {
                                    def.resolve();
                                });
                            });
                        });
                    });
                });
            });
        }
        return def.promise();
    }

    private ask = () => {
        var o = this;
        var index: number = o.swiper.activeIndex;
        var s = o.m.lang().speechSynth;
        var synth = o.m.lang().mmse.synth;
        var q = o.m.lang().mmse.questions[index];
        var isLastIndex = index === o.m.lang().mmse.questions.length - 1;
        var question = (q.synthIntro || "") + (q.synthTask || q.title);
        question = question.replace(/<\/?[^>]+(>|$)/g, "");
        var questionText = (isLastIndex ? "" : synth.questionNumber + " " + (index + 1) + ". -w600- ") + question + " -w500- ";
        speechSynth.play(s.lang, s.id, s.rate, s.pitch, questionText).done(() => {
        });
    }

    private highlightBullets = () => {
        var bullets = $('.mmse .swiper-pagination-bullet');
        var xy0 = $(bullets[0]).offset();
        var xy1 = $(bullets[bullets.length - 1]).offset();
        var bulWidth = $(bullets[bullets.length - 1]).width();
        var width = xy1.left - xy0.left + bulWidth;
        var height = bullets.height();
        return intro.show(xy0.left, xy0.top, width, height, 10);
    }

    private highlightQuestion = () => {
        var el = $('.mmse .swiper-slide-active .testQuestionMMSE');
        var xy = el.offset();
        return intro.show(xy.left, xy.top, el.outerWidth(), el.outerHeight(), 10);
    }

    private highlightEmergency = () => {
        var el = $('.mmse .callHelpBtn');
        var xy = el.offset();
        return intro.show(xy.left, xy.top, el.outerWidth(), el.outerHeight(), 10);
    }

}
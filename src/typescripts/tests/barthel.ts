class Barthel {

    private swiper;
    private speechRec: SpeechRec;

    public constructor(private m) {
        var o = this;
        o.setF7EventHooks();
    }

    public destructor = () => {
        var o = this;
        o.speechRec.stop();
    }

    public selectAnswer = (index: number) => {
        var o = this;
        return () => {
            var cards = $(o.swiper.slides[o.swiper.activeIndex]).find('.testAnswers .tiCard');
            cards.removeClass('tiCardSelected');
            $(cards[index]).addClass('tiCardSelected');
            var s = o.m.lang().speechSynth;
            var synth = o.m.lang().barthelIndexNormal.synth;
            var q = o.m.lang().barthelIndexNormal.questions[o.swiper.activeIndex];
            var text = synth.selectedAnswer + " " + (index + 1) + ". -w300- " + q.answers[index].text;
            speechSynth.play(s.lang, s.id, s.rate, s.pitch, text).done(() => {
            });
        };
    }

    public back = () => {
        var o = this;
        var s = o.m.lang().speechSynth;
        var synth = o.m.lang().barthelIndexNormal.synth;
        if (o.swiper.activeIndex > 0) {
            speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.goPreviousBtn).done(() => {
                o.swiper.slidePrev();
            });
        } else {
            mainView.router.back();
            o.destructor();
        }
    }

    public goNext = () => {
        var o = this;
        return () => {
            var s = o.m.lang().speechSynth;
            var synth = o.m.lang().barthelIndexNormal.synth;
            var isSelected = false;
            var cards = $(o.swiper.slides[o.swiper.activeIndex]).find('.testAnswers .tiCard');
            cards.each((i, el) => {
                isSelected = isSelected || $(el).hasClass('tiCardSelected');
            });
            if (isSelected) {
                speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.goNextBtn).done(() => {
                    o.swiper.slideNext();
                });
            } else {
                app.popup(".popup-ChooseOption");
            }

        };
    }

    private setF7EventHooks = () => {
        var o = this;
        app.onPageInit('barthelIndexNormal', (page) => {
            o.onPageInit();
        });
        app.onPageAfterAnimation('barthelIndexNormal', (page) => {
            o.onPageAfterAnimation();
        });
        $('.popup-ChooseOption').on('opened', () => {
            var s = o.m.lang().speechSynth;
            speechSynth.play(s.lang, s.id, s.rate, s.pitch, o.m.lang().chooseOption).done(() => {
                setTimeout(() => {
                    app.closeModal();
                }, 500);
            });
        });
    }

    private onPageInit = () => {
        var o = this;
        o.swiper = app.swiper('.barthelIndexNormal .swiper-container', {
            pagination: '.barthelIndexNormal .swiper-pagination',
            paginationHide: false,
            paginationClickable: false,
            onlyExternal: true,
            speed: 500
        });
        o.swiper.on("slideChangeEnd", o.onSlideChangeEnd);
        o.speechRec = new SpeechRec(o.m);
        o.speechRec.onNotUnderstanding = (keywords) => {
            var s = o.m.lang().speechSynth;
            var synth = o.m.lang().barthelIndexNormal.synth;
            speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.notUnderstanding).done(() => {
            });
        }
        speechSynth.speechRec = o.speechRec;
        o.speechRec.start();
    }

    private onPageAfterAnimation = () => {
        var o = this;
        o.welcomeSynth().then(() => {
            o.ask(true);
        });
        //o.ask(true);
        o.initSynthCommands();
    }

    private initSynthCommands = () => {
        var o = this;
        var commands = o.m.lang().barthelIndexNormal.synthCommands;
        o.speechRec.subscribe("barthelSynthCommands", commands, (commandId) => {
            switch (commandId[0]) {
                case "nextQuestion":
                    o.goNext()();
                    break;
                case "previousQuestion":
                    o.back();
                    break;
                case "repeatQuestion":
                    o.ask(false);
                    break;
            }
            console.log("command detected: " + commandId);
        });
    }

    private onSlideChangeEnd = () => {
        var o = this;
        o.ask(o.swiper.activeIndex < 1);
    }

    private welcomeSynth = () => {
        var o = this;
        var def = $.Deferred();
        if (o.m.lang().speechSynth) {
            var s = o.m.lang().speechSynth;
            var synth = o.m.lang().barthelIndexNormal.synth;

            speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.welcome).done(() => {
                intro.show(0, 0, 0, 0, 0);
                speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.welcome2).done(() => {
                    o.highlightBullets();
                    speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.bullets).done(() => {
                        o.highlightQuestion();
                        speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.question).done(() => {
                            o.highlightAnswers();
                            speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.answers).done(() => {
                                o.highlightGoNext();
                                speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.goNext).done(() => {
                                    o.highlightEmergency();
                                    speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.emergency).done(() => {
                                        intro.hide();
                                        speechSynth.play(s.lang, s.id, s.rate, s.pitch, synth.ready).done(() => {
                                            def.resolve();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        }
        return def.promise();
    }

    private ask = (highlight: boolean) => {
        var o = this;
        console.log("ask");
        var index: number = o.swiper.activeIndex;
        var s = o.m.lang().speechSynth;
        var synth = o.m.lang().barthelIndexNormal.synth;
        var q = o.m.lang().barthelIndexNormal.questions[index];
        var question = q.question.replace(/<\/?[^>]+(>|$)/g, "");
        var questionText = synth.questionNumber + " " + (index + 1) + ". -w600- " + question + " -w500- ";
        if (highlight) {
            o.highlightQuestion();
        }
        speechSynth.play(s.lang, s.id, s.rate, s.pitch, questionText).done(() => {
            var answersText = synth.possibleAnsers + ".";
            (<any[]>q.answers).forEach((a) => {
                answersText = answersText + " -w1000- " + a.text;
            });
            if (highlight) {
                o.highlightAnswers();
            }
            speechSynth.play(s.lang, s.id, s.rate, s.pitch, answersText).done(() => {
                intro.hide();
                var speechAnswers: any = {};
                (<any[]>q.answers).forEach((a, i) => {
                    speechAnswers[i] = a.speechRecAnswers;
                })
                o.speechRec.subscribe("barthelAsk", speechAnswers, (ids) => {
                    o.selectAnswer(parseInt(ids[0]))();
                });
            });
        });
    }

    private highlightBullets = () => {
        var bullets = $('.barthelIndexNormal .swiper-pagination-bullet');
        var xy0 = $(bullets[0]).offset();
        var xy1 = $(bullets[bullets.length - 1]).offset();
        var bulWidth = $(bullets[bullets.length - 1]).width();
        var width = xy1.left - xy0.left + bulWidth;
        var height = bullets.height();
        return intro.show(xy0.left, xy0.top, width, height, 10);
    }

    private highlightQuestion = () => {
        var el = $('.barthelIndexNormal .swiper-slide-active .testQuestion');
        var xy = el.offset();
        return intro.show(xy.left, xy.top, el.outerWidth(), el.outerHeight(), 10);
    }

    private highlightAnswers = () => {
        var el = $('.barthelIndexNormal .swiper-slide-active .testAnswers');
        var xy = el.offset();
        return intro.show(xy.left, xy.top, el.outerWidth(), el.outerHeight(), 10);
    }

    private highlightEmergency = () => {
        var el = $('.barthelIndexNormal .callHelpBtn');
        var xy = el.offset();
        return intro.show(xy.left, xy.top, el.outerWidth(), el.outerHeight(), 10);
    }

    private highlightGoNext = () => {
        var el = $('.barthelIndexNormal .testGoNext');
        var xy = el.offset();
        return intro.show(xy.left, xy.top, el.outerWidth(), el.outerHeight(), 10);
    }

    private wait = (ms: number) => {
        var def = $.Deferred<void>();
        setTimeout(function () {
            def.resolve();
        }, ms);
        return def.promise();
    }

}
var mmsePoints = [//1, 
    //1, 1, 1, 1, 1, 1, 1,
    1, 1, 3, 5, 3, 2, 1, 3, 1, 1, 1];

function getSeason() {
    var month = new Date().getMonth() + 1;
    if (month == 1 || month == 2 || month == 12) {
        return 'Winter';
    } else if (month == 3 || month == 4 || month == 5) {
        return 'Spring';
    } if (month == 6 || month == 7 || month == 8) {
        return 'Summer';
    } if (month == 9 || month == 10 || month == 11) {
        return 'Fall';
    }
}

function getDay() {
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return days[new Date().getDay()];
}

function getMonth() {
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    return months[new Date().getMonth()];
}

var mmseAnswers = [
    // () => { return new Date().getFullYear().toString(); },
    getSeason,
    // () => { return new Date().getDate().toString(); },
    //getDay,
    // getMonth,
    // () => { return "Spain"; },
    // () => { return "Catalonia"; },
    () => { return "Barcelona"; },
    // () => { return "Hospital Sant Antoni ABAT"; },
    // () => { return "First floor"; },
    () => { return "table, robot, chair"; },
    () => { return "100, 93, 86, 79, 72, 65, 58, 51"; },
    () => { return "table, robot, chair"; },
    () => { return "pencil, watch"; },
    () => { return "No ifs, ands, or buts"; },
    () => { return "-"; },
    () => { return "-"; },
    () => { return "-"; },
    () => { return "-"; }
];

class MmseDoctor {

    private tasks = [];
    private task = ko.observable<any>();
    private totalPoints = ko.observable("");

    public constructor(private m) {
        var o = this;
        o.setF7EventHooks();
    }

    private listenToPubNub = () => {
        var o = this;
        pubnub.subscribe({
            channel: 'index',
            callback: function (index) {
                if (typeof index === "number" && index > 0) {
                    app.hidePreloader();
                    o.loadPage(index - 1);
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    }

    private setF7EventHooks = () => {
        var o = this;
        app.onPageInit('mmseDoctor', (page) => {
            o.onPageInit();
        });
    }

    private onPageInit = () => {
        var o = this;
        o.initData();
        o.listenToPubNub();
    }

    public previousTask = () => {
        app.showPreloader("Please Wait...");
        pubnub.publish({
            channel: 'command',
            message: "previous"
        });
    }

    public nextTask = () => {
        app.showPreloader("Please Wait...");
        pubnub.publish({
            channel: 'command',
            message: "next"
        });
    }

    public onPointChange = (_this_) => {
        var o = this;
        o.task().selected(_this_.value);
    }

    private loadPage = (index: number) => {
        var o = this;
        var len = Math.min(o.m.lang().mmse.questions.length, mmseAnswers.length);
        if (index < len) {
            o.task(o.tasks[index]);
        } else {
            var points = 0;
            o.tasks.forEach((t) => {
                points += parseInt(t.selected()) || 0;
            });
            o.totalPoints(o.m.lang().mmseDoctor.totalPoints + points);
            app.popup(".popup-mmseDoctorFinished");
        }
    }

    private initData = () => {
        var o = this;
        o.tasks = [];
        var len = Math.min(o.m.lang().mmse.questions.length, mmseAnswers.length);
        for (var i = 0; i < len; i++) {
            var ps = [];
            for (var j = 0; j <= mmsePoints[i]; j++) {
                ps.push({
                    text: j + " points",
                    value: j
                });
            }
            o.tasks.push({
                question: o.m.lang().mmse.questions[i].title,
                answer: mmseAnswers[i](),
                points: ps,
                selected: ko.observable(false)
            });
        }
    }
}
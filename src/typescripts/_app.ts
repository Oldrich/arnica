var app, mainView;
var model: Model;
var speechSynth: SpeechSynth;
var intro: Intro;
var pubnub;

$(document).ready(() => {

    pubnub = PUBNUB({
        ssl: (('https:' == document.location.protocol) ? true : false),
        subscribe_key: 'sub-c-7e2c5a0e-41d2-11e6-bfbb-02ee2ddab7fe', // always required
        publish_key: 'pub-c-bf7fa1d9-d42f-4fe8-bb47-2f507a45cf15' // only required if publishing
    });

    app = new Framework7({
    });

    mainView = app.addView('.view-main', {
        dynamicNavbar: false,
        domCache: true
    });

    model = new Model();
    ko.applyBindings(model);

    speechSynth = new SpeechSynth();

    intro = new Intro();

    $('.popup-callHelp').on('opened', () => {
        var s = model.lang().speechSynth;
        var synth = model.lang().barthelIndexNormal.synth;
        speechSynth.play(s.lang, s.id, s.rate, s.pitch, model.lang().callHelp.areYouSure).done(() => {
            var answersText = synth.possibleAnsers + ". -w600- " + model.lang().callHelp.yes + " -w600- " + model.lang().callHelp.no;
            speechSynth.play(s.lang, s.id, s.rate, s.pitch, answersText).done(() => {
            });
        });
    });
});

ko.bindingHandlers['applyWhen'] = {
    init: function () {
        return { controlsDescendantBindings: true };
    },
    update: function (element, valueAccessor, allBindings, model, bindingContext) {
        var isOK = Boolean(ko.unwrap(valueAccessor()));
        if (isOK && !element.bindingApplied) {
            element.bindingApplied = true;
            ko.applyBindingsToDescendants(bindingContext, element);
        } else if (!isOK && element.bindingApplied) {
            ko.removeNode(element);
            element.bindingApplied = false;
        }
    }
};
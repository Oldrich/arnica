class FireWorks {

    private context;
    private particles = [];
    private rockets: FireworksRocket[] = [];
    private MAX_PARTICLES = 400;
    private colorCode = 0;

    public constructor(private canvas: HTMLCanvasElement) {
        var o = this;
        o.context = canvas.getContext('2d');
        setInterval(o.launch, 800);
        setInterval(o.loop, 1000 / 50);
    }

    private launch = () => {
        var o = this;
        var xy = {
            x: Math.random() * o.canvas.width,
            y: Math.random() * o.canvas.height
        };
    }

    private launchFrom = (xy: { x: number; y: number }) => {
        var o = this;
        if (o.rockets.length < 10) {
            var rocket = new FireworksRocket(xy);
            rocket.explosionColor = Math.floor(Math.random() * 360 / 10) * 10;
            rocket.vel.y = Math.random() * -3 - 4;
            rocket.vel.x = Math.random() * 6 - 3;
            rocket.size = 8;
            rocket.shrink = 0.999;
            rocket.gravity = 0.01;
            o.rockets.push(rocket);
        }
    }

    private loop = () => {
        var o = this;
        o.context.fillStyle = "rgba(0, 0, 0, 0.05)";
        o.context.fillRect(0, 0, o.canvas.width, o.canvas.height);

        var existingRockets = [];

        for (var i = 0; i < o.rockets.length; i++) {
            o.rockets[i].update();
            o.rockets[i].render(o.context);

            var randomChance = o.rockets[i].pos.y < (o.canvas.height * 2 / 3) ? (Math.random() * 100 <= 1) : false;

            /* Explosion rules
                         - 80% of screen
                        - going down
                        - close to the mouse
                        - 1% chance of random explosion
                    */
            if (o.rockets[i].pos.y < o.canvas.height / 5 || o.rockets[i].vel.y >= 0 || randomChance) {
                o.rockets[i].explode(o.particles);
            } else {
                existingRockets.push(o.rockets[i]);
            }
        }

        o.rockets = existingRockets;

        var existingParticles = [];

        for (var i = 0; i < o.particles.length; i++) {
            o.particles[i].update();

            if (o.particles[i].exists()) {
                o.particles[i].render(o.context);
                existingParticles.push(o.particles[i]);
            }
        }

        o.particles = existingParticles;

        while (o.particles.length > o.MAX_PARTICLES) {
            o.particles.shift();
        }
    }





}
interface LangInfo {
  name: string;
  id: string
}

class Lang {

  public langID: KnockoutObservable<string>;
  public langList: LangInfo[];
  public lang: KnockoutComputed<any>;

  public constructor() {
    var o = this;
    o.langID = ko.observable("en");
    o.langList = o.getSortedLanugages();
    o.lang = ko.computed(() => { return languages[o.langID()]; });
    o.watchLangID();
  }

  private watchLangID = () => {
    var o = this;
    o.langID.subscribe((langID) => {
    });
  }

  private getSortedLanugages() {
    var langList: LangInfo[] = [];
    for (var key in languages) {
      langList.push({ name: languages[key]["name"], id: key });
    }
    langList = langList.sort((a, b) => {
      if (a.name > b.name) {
        return 1;
      } else if (a.name < b.name) {
        return -1;
      } else {
        return 0;
      }
    });
    return langList;
  }

}
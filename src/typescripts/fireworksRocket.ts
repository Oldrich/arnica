class FireworksRocket extends FireworksParticle {

    public explosionColor = 0;

    public constructor(xy: { x: number; y: number }) {
        super(xy);
    }

    public explode = (particles: FireworksParticle[]) => {
        var o = this;
        var count = Math.random() * 10 + 80;

        for (var i = 0; i < count; i++) {
            var particle = new FireworksParticle(o.pos);
            var angle = Math.random() * Math.PI * 2;

            // emulate 3D effect by using cosine and put more particles in the middle
            var speed = Math.cos(Math.random() * Math.PI / 2) * 15;

            particle.vel.x = Math.cos(angle) * speed;
            particle.vel.y = Math.sin(angle) * speed;

            particle.size = 10;

            particle.gravity = 0.2;
            particle.resistance = 0.92;
            particle.shrink = Math.random() * 0.05 + 0.93;

            particle.flick = true;
            particle.color = this.explosionColor;

            particles.push(particle);
        }
    };

    public render = (c) => {
        var o = this;
        if (!o.exists()) {
            return;
        }

        c.save();

        c.globalCompositeOperation = 'lighter';

        var x = o.pos.x,
            y = o.pos.y,
            r = o.size / 2;

        var gradient = c.createRadialGradient(x, y, 0.1, x, y, r);
        gradient.addColorStop(0.1, "rgba(255, 255, 255 ," + o.alpha + ")");
        gradient.addColorStop(1, "rgba(0, 0, 0, " + o.alpha + ")");

        c.fillStyle = gradient;

        c.beginPath();
        c.arc(o.pos.x, o.pos.y, o.flick ? Math.random() * o.size / 2 + o.size / 2 : o.size, 0, Math.PI * 2, true);
        c.closePath();
        c.fill();

        c.restore();
    };
}
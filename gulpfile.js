var gulp = require('gulp');
var concat = require('gulp-concat');
var handlebars = require('gulp-compile-handlebars');
var rimraf = require('gulp-rimraf');
var ts = require('gulp-typescript');
var replace = require('gulp-replace');
var runSequence = require('run-sequence');
var ftp = require('vinyl-ftp');
var autoprefixer = require('gulp-autoprefixer');
var gulpS3 = require('gulp-s3-upload');

var s3 = gulpS3({
    accessKeyId: "AKIAIK6QC52SHWON53KQ",
    secretAccessKey: "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"
});

var base = 'build/web/';

var vendorFontFiles = [
    'node_modules/font-awesome/fonts/*',
    'node_modules/ionicons/fonts/*'
];

var vendorJSFiles = [
    'node_modules/jquery/dist/jquery.js',
    'node_modules/framework7/dist/js/framework7.js',
    'node_modules/knockout/build/output/knockout-latest.js',
    'node_modules/moment/min/moment.min.js'
];

var vendorCSSFiles = [
    'node_modules/framework7/dist/css/framework7.ios.css',
    'node_modules/framework7/dist/css/framework7.ios.colors.css',
    'node_modules/font-awesome/css/font-awesome.min.css',
    'node_modules/ionicons/css/ionicons.min.css',
];

var cssFiles = [
    'src/**/*.css'
];

var tsFiles = [
    'typings/**/*.d.ts',
    'src/**/*.ts'
];

var tsProject = ts.createProject({
    noExternalResolve: true,
    target: 'ES5'
});

gulp.task('clean', function () {
    return gulp.src(base).pipe(rimraf());
});

gulp.task('other', function (done) {
    return gulp.src('src/other/**/*.*')
        .pipe(gulp.dest(base));
});

gulp.task('vendorFonts', function () {
    return gulp.src(vendorFontFiles)
        .pipe(gulp.dest(base + "/fonts"));
});

gulp.task('vendorJS', function () {
    return gulp.src(vendorJSFiles)
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(base));
});

gulp.task('vendorCSS', function () {
    return gulp.src(vendorCSSFiles)
        .pipe(concat('vendor.css'))
        .pipe(replace(/\.\.\/fonts\//g, './fonts/'))
        .pipe(gulp.dest(base));
});

gulp.task('templates', function () {
    return gulp.src('src/templates/index.html')
        .pipe(handlebars({}, { batch: ['./src/templates/pages'] }))
        .pipe(gulp.dest(base));
});

gulp.task('ts', function () {
    return gulp.src(tsFiles)
        .pipe(ts(tsProject))
        .js.pipe(concat('app.js'))
        .pipe(gulp.dest(base))
});

gulp.task('css', function () {
    return gulp.src(cssFiles)
        .pipe(concat('app.css'))
        // .pipe(autoprefixer({
        //     browsers: ['last 2 versions'],
        //     cascade: false
        // }))
        .pipe(gulp.dest(base))
});

gulp.task('watch', function () {

    var files = cssFiles.concat(tsFiles).concat(['src/templates/**/*.html']);

    gulp.watch(files, { debounceDelay: 400 }, function () {
        runSequence(['ts', 'css', 'templates']);
    });

});

gulp.task('watch-publish', function () {

    var files = cssFiles.concat(tsFiles).concat(['src/templates/**/*.html']);

    gulp.watch(files, { debounceDelay: 400 }, function () {
        runSequence(['ts', 'css', 'templates'], 'upload');
    });

});

gulp.task('upload', function () {
    var conn = ftp.create({
        host: 'osv.dti.dk',
        user: 'osv',
        password: 'pLKh4%!4',
        parallel: 1,
        //log: gutil.log
    });

    return gulp.src(base + '**/*', { base: base, buffer: false })
        //.pipe(conn.newer('/osv/ble'))
        .pipe(conn.dest('/osv/arnica'));
});

gulp.task('upload-s3', function () {
    return gulp.src(base + '**')
        .pipe(s3({
            Bucket: 'arnica',
            ACL: 'public-read'
        }, {
                maxRetries: 5
            }));
});

gulp.task('default', function () {
    return runSequence('clean', ['vendorFonts', 'vendorJS', 'vendorCSS', 'ts', 'css', 'templates', 'other']);
});

gulp.task('publish', function () {
    return runSequence('clean', ['vendorFonts', 'vendorJS', 'vendorCSS', 'ts', 'css', 'templates', 'other'], 'upload');
});